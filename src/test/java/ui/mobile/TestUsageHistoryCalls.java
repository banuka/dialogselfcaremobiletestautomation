package ui.mobile;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

/**
 * @author Banuka Liyanage created on 10/31/2017
 */
public class TestUsageHistoryCalls extends TestBase {

    @Test(groups = {"USAGEHISTORY"})
    public void test_UsageHistoryCalls_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .selectMobileNo("777334187")
                .navigateToUsageHistory()
                .getCallsHistroy();
    }
}

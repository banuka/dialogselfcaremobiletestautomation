package ui.mobile;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

/**
 * @author Banuka Liyanage created on 11/2/2017
 */
public class TestUsageHistorySMS extends TestBase {

    @Test(groups = {"HISTORYSMS"})
    public void test_UsageHistorySMS_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .selectMobileNo("777334187")
                .navigateToUsageHistory()
                .clickOnSMS()
                .getSMSHitory();
    }
}

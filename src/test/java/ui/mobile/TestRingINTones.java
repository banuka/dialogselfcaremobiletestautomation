package ui.mobile;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

/**
 * @author Banuka Liyanage created on 11/6/2017
 */
public class TestRingINTones extends TestBase {

    @Test(groups = {"RINGTONES", "SKIP"})
    public void test_RingTones_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .navigateToRingINTones()
                .setSelectionCriteria("Artist")
                .selectItem("BNS")
                .navigateToSubscribeRingTone("Sara Sihina (Instrumental)")
                .clickSubscribe()
                .clickConfirm()
                .verifySuccessFullMessage("Successfully sent the subscription request")
                .clickOK();
//                .navigateToMyTones()
//                .verifyToneIsListed("Sara Sihina (Instrumental)");
    }
}

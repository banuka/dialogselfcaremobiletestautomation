package ui.mobile;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;
import utils.support.SOAPClient;

import java.util.LinkedHashMap;
import java.util.Map;

import static pages.BasePage.getSharedPage;

/**
 * @author Banuka Liyanage created on 10/22/2017
 */
public class TestBillInfo extends TestBase {

    @Test(groups = {"MOBBILLINFO"})
    public void test_BillInfo_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .navigateToBillInfo()
                .selectMobileNo("772461403");
    }

    @Test(groups = {"MOBBILLINFO"})
    public void test_BillInfo_002() throws FrameworkException{
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .selectMobileNo("777334187")
                .navigateToBillInfo()
                .clickOnPaymentHistory()
                .getPaymentHistory();
    }

    @Test(groups = {"MOBBILLINFO"})
    public void test_BillInfo_003() throws FrameworkException{
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .selectMobileNo("777334187")
                .navigateToBillInfo()
                .verfyBillInfoDetails("777334187");
    }

//    public static void main(String[] args) {
//
//        Map<String, Object> requestParams = new LinkedHashMap<String, Object>();
//        requestParams.put("p_mobile_no", "777334187");
//        SOAPClient soapClient = new SOAPClient("http://172.26.29.29:8280/services/OmibioService.OmibioServiceHttpSoap11Endpoint?wsdl", "getBillInfo", "http://crm.dialog.lk/getBillingInfo");
//
//        soapClient.setPREFERRED_PREFIX("soapenv");
//        soapClient.setNAMESPACE("ccb");
//        soapClient.setNAMESPACE_URI("http://crm.dialog.lk/CCBSWSBible/");
//        soapClient.getResponse(requestParams, true);
//    }


//    public static void main(String[] args) {
//
//        SOAPClient soapClient = new SOAPClient("http://172.26.34.91:8280/services/DialogServices.DialogServicesHttpSoap11Endpoint?wsdl", "getCustomerDetails", "http://cmu.dialog.lk/DialogServices/getCustomerDetails");
//        soapClient.setPREFERRED_PREFIX("soapenv");
//        soapClient.setNAMESPACE("dial");
//        soapClient.setNAMESPACE_URI("http://cmu.dialog.lk/DialogServices/");
//
//        Map<String, Object> requestParams = new LinkedHashMap<String, Object>();
//        requestParams.put("refAccount", "777334187");
//        requestParams.put("LOB", "GSM");
//        requestParams.put("Token", "456");
//        requestParams.put("IMEI", "9402970236468959");
//        Map<String, Object> responseParams = soapClient.getResponse(requestParams, false);
//        System.out.println("The End.");
//    }



//    public static void main(String[] args) {
//
//        SOAPClient soapClient = new SOAPClient("http://172.26.34.91:8280/services/DialogServices.DialogServicesHttpSoap11Endpoint?wsdl",
//                "getBillPaymentReminder", "http://cmu.dialog.lk/DialogServices/getBillPaymentReminder");
//        soapClient.setPREFERRED_PREFIX("soapenv");
//        soapClient.setNAMESPACE("dial");
//        soapClient.setNAMESPACE_URI("http://cmu.dialog.lk/DialogServices/");
//
//        Map<String, Object> requestParams = new LinkedHashMap<String, Object>();
//        requestParams.put("refAccount", "777334187");
//        requestParams.put("LOB", "GSM");
//        requestParams.put("token", "456");
//        requestParams.put("IMEI", "9402970236468959");
//        soapClient.getResponse(requestParams, false);
//    }

    public static void main(String[] args) {

        SOAPClient soapClient = new SOAPClient("http://172.26.29.29:8280/services/OmibioService.OmibioServiceHttpSoap11Endpoint?wsdl",
                "getCreditLimitForContractId", "http://crm.dialog.lk/getCreditLimitForContractId");
        soapClient.setPREFERRED_PREFIX("soapenv");
        soapClient.setNAMESPACE("ccb");
        soapClient.setNAMESPACE_URI("http://crm.dialog.lk/CCBSWSBible/");

        Map<String, Object> requestParams = new LinkedHashMap<String, Object>();
        requestParams.put("p_contract_id", "33569751");
        Map<String, Object> responseParams = soapClient.getResponse(requestParams, true);
        System.out.println("The End.");
    }
}

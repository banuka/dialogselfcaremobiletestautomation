package ui.mobile;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

/**
 * @author Banuka Liyanage created on 10/24/2017
 */
public class TestMyProfile extends TestBase {

    @Test(groups = {"MOBMYPROFILE"})
    public void test_MyProfile_001()throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .selectMobileNo("777334187")
                .navigateToMyProfile()
                .getMyProfileDetails()
                .verifyMyProfileDetails();
    }
}

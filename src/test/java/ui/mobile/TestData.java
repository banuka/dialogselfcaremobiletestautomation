package ui.mobile;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

/**
 * @author Banuka Liyanage created on 10/30/2017
 */
public class TestData extends TestBase {

    @Test(groups = {"MOBDATA"})
    public void test_Data_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToMobile()
                .navigateToData();
    }
}

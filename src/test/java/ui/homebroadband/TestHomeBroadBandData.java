package ui.homebroadband;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

public class TestHomeBroadBandData extends TestBase {

    @Test(groups = {"HBDATA"})
    public void test_HomeBroadBandData_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToHomeBroadBandFixedLine()
                .navigateToData()
                .getHomeBroadUsageDetails();
    }

    @Test(groups = {"HBDATA"})
    public void test_HomeBroadBandData_002() throws FrameworkException{
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToHomeBroadBandFixedLine()
                .navigateToData()
                .clickHistoryPage()
                .getDataHistory();
    }
}

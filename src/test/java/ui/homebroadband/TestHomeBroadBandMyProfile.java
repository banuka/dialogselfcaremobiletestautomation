package ui.homebroadband;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

public class TestHomeBroadBandMyProfile extends TestBase {

    @Test(groups = {"HBMYPROFILE"})
    public void test_HomeBroadBandMyProfile_001()throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToHomeBroadBandFixedLine()
                .navigateToMyProfile()
                .getMyProfileDetails();
    }
}

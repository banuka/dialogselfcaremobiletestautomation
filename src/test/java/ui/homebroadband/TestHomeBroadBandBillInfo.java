package ui.homebroadband;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

public class TestHomeBroadBandBillInfo extends TestBase {

    @Test(groups = {"HBBILLINFO"})
    public void test_HomeBroadBandBillInfo_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToHomeBroadBandFixedLine()
                .navigateToBillInfo()
                .getBillInfoDetails();
    }

}

package ui.loyalty;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

public class TestStarPoints extends TestBase {

    @Test(groups = {"STARPOINTS","SKIP"})
    public void test_StarPoints_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToLoyalty()
                .selectMobileNo("777334187")
                .clickStarPoints()
                .navigateToBalance()
                .getBalanceDetails()
                .clickBackBtn()
                .navigateToStarPointsTransfer()
                .getPreRedeemableBalance() //764.48
                .transferStarPointsMobile("772461403", "1")
                .verifyTotalRedeemableBalanceReduce(1)
                .clickBackBtn()
                .navigateToBalance()
                .verifyTotalRedeemableBalanceReduce(1)
                .verifyTotalBalanceReduce(1);
    }

}

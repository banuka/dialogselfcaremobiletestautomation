package ui.settings;

import Exceptions.FrameworkException;
import org.testng.annotations.Test;
import pages.HomePage;
import ui.TestBase;
import utils.SharedObjectKeys;

import static pages.BasePage.getSharedPage;

/**
 * @author Banuka Liyanage created on 1/26/2018
 */
public class TestManageAccounts extends TestBase{

    @Test(groups = {"SETTINGS"})
    public void test_StarPoints_001() throws FrameworkException {
        getSharedPage(SharedObjectKeys.HOME_PAGE, HomePage.class)
                .navigateToSettings()
                .navigateToMyAccounts();
    }
}

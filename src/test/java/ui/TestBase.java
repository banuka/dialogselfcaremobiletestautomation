package ui;

import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.BasePage;
import pages.HomePage;
import tests.BaseTest;
import utils.SharedObjectKeys;
import utils.support.TestStrategy;

import java.io.IOException;

import static pages.BasePage.setSharedPage;
import static utils.driver.DriverConnection.closeDriver;

/**
 * @author Banuka Liyanage created on 12/29/2017
 */
public class TestBase extends BaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestBase.class);
    private static boolean testSuiteExecutionCompleted = false;

    public TestBase(){
        super(TestStrategy.ANDROIDNATIVEREALDEVICE);
    }

    @BeforeSuite(alwaysRun = true)
    public void setUp(){
        HomePage homePage = new HomePage(BasePage.driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), homePage);
        setSharedPage(SharedObjectKeys.HOME_PAGE, homePage);
    }



    @AfterMethod(alwaysRun = true)
    public void navigateBackToHome(){
        if(!testSuiteExecutionCompleted && driver != null) {
            try {
                LOGGER.warn("Navigating back to Home screen");
//                Thread.sleep(2500);
                (new TouchAction(driver)).longPress(36, 95).perform().release().perform();
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                LOGGER.error("Navigated to Home screen failed due to : " + e);
            }
        }
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() throws SessionNotCreatedException, IOException, InterruptedException {
        testSuiteExecutionCompleted = true;
        closeDriver();
    }
}

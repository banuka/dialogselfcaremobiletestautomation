package pages.loyalty;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pages.BasePage;
import pages.ConfirmationDialog2;

public class StarPointsTransferPage extends BasePage {

    private static final Logger LOGGER = LoggerFactory.getLogger(StarPointsTransferPage.class);

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"From Number*\"]/following-sibling::android.widget.EditText")
    private MobileElement fromNumberLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Transfer To*\"]/following-sibling::android.widget.EditText")
    private MobileElement transferToDropDown;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"To Number*\"]/following-sibling::android.widget.EditText")
    private MobileElement toNumberLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Points*\"]/following-sibling::android.widget.EditText")
    private MobileElement pointsLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[contains(@content-desc, 'Your total redeemable balance is:')]")
    private MobileElement redeemableBalanceLbl;

    @FindBy(how = How.XPATH, using = "(//android.view.View[@content-desc=\"Transfer\"])[2]")
    private MobileElement transferBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private MobileElement backBtn;


    public StarPointsTransferPage(AppiumDriver driver) {
        super(driver);
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Points*\"]/following-sibling::android.widget.EditText"));
    }


    public StarPointsTransferPage getPreRedeemableBalance() {
        double preRedeemableBalance = Double.parseDouble(redeemableBalanceLbl.getAttribute("name").split(":")[1].trim());
        globalVariables.put("preRedeemableBalance", preRedeemableBalance);

        LOGGER.info("Store reedeemable Balance details to global variable - preRedeemableBalance." + "****" + preRedeemableBalance + "****");
        return this;
    }

    public StarPointsTransferPage transferStarPointsMobile(String number, String amount){
        selectDropDownItem(transferToDropDown, "Mobile Number");
        toNumberLbl.sendKeys(number);
        pointsLbl.sendKeys(amount);

        if(driver instanceof AndroidDriver) {
            ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.BACK);
        }
        transferBtn.click();
        ConfirmationDialog2 confirmationDialog2 = new ConfirmationDialog2(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), confirmationDialog2);

        confirmationDialog2.clickNoBtn();
        return this;
    }


    public StarPointsTransferPage verifyTotalRedeemableBalanceReduce(double amount){
        double expAmount = (Double) globalVariables.get("preRedeemableBalance") - amount;
        double actAmount = Double.parseDouble(redeemableBalanceLbl.getAttribute("name").split(":")[1].trim());

//        Assert.assertEquals(actAmount, expAmount, "Total redeemable balance not reduce accurately.");

        return this;
    }


    public StarPointsPage clickBackBtn(){
        backBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        StarPointsPage starPointsPage = new StarPointsPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), starPointsPage);
        return starPointsPage;
    }
}

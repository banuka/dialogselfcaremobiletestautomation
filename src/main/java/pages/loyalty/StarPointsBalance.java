package pages.loyalty;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pages.BasePage;

import java.util.HashMap;

public class StarPointsBalance extends BasePage {
    AppiumDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(StarPointsBalance.class);

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Account #\"]/following-sibling::android.widget.EditText")
    private MobileElement amountNoLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Total Balance\"]/following-sibling::android.widget.EditText")
    private MobileElement totalBalanceLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Redeemable Balance\"]/following-sibling::android.widget.EditText")
    private MobileElement redeemableBalance;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private MobileElement backBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Refresh\"]")
    private MobileElement refreshBtn;


    public StarPointsBalance(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Redeemable Balance\"]/following-sibling::android.widget.EditText"));
    }

    public StarPointsBalance getBalanceDetails() {
        HashMap<String, String> spBalance = new HashMap<String, String>();

        spBalance.put("Amount #", amountNoLbl.getAttribute("name"));
        spBalance.put("Total Balance", totalBalanceLbl.getAttribute("name"));
        spBalance.put("redeemableBalance", redeemableBalance.getAttribute("name"));

        globalVariables.put("SPBalanceMap", spBalance);

        LOGGER.info("Store StarPoint Balance details to global variable - SPBalanceMap." + "****" + spBalance + "****");
        return this;
    }


    public StarPointsBalance verifyTotalRedeemableBalanceReduce(double amount){
        double expAmount = Double.parseDouble(((HashMap<String, String>)globalVariables.get("SPBalanceMap")).get("redeemableBalance")) - amount;
        double actAmount = Double.parseDouble(redeemableBalance.getAttribute("name"));

        Assert.assertEquals(actAmount, expAmount, "Total redeemable balance not reduce accurately.");

        LOGGER.info("Verification done for the reedeemable balance deduction.");
        return this;
    }

    public StarPointsBalance verifyTotalBalanceReduce(double amount){
        double expAmount = Double.parseDouble(((HashMap<String, String>)globalVariables.get("SPBalanceMap")).get("Total Balance")) - amount;
        double actAmount = Double.parseDouble(totalBalanceLbl.getAttribute("name"));

        Assert.assertEquals(actAmount, expAmount, "Total balance not reduce accurately.");

        LOGGER.info("Verification done for the total balance deduction.");
        return this;
    }

    public StarPointsPage clickBackBtn(){
        backBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        StarPointsPage starPointsPage = new StarPointsPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), starPointsPage);

        LOGGER.info("Click Back on Star Points Balance.");
        return starPointsPage;
    }


}

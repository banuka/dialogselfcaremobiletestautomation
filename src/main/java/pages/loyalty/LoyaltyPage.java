package pages.loyalty;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;
import pages.HomePage;
import pages.MobilePage;
import pages.PageBase;

public class LoyaltyPage extends PageBase{
    AppiumDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoyaltyPage.class);

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText")
    private MobileElement mobileNoDropDown;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Tier Points\"]")
    private MobileElement tierPointsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Star Points\"]")
    private MobileElement starPointsBtn;

    public LoyaltyPage(AppiumDriver driver) throws FrameworkException {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Star Points\"]"));
    }

    public LoyaltyPage selectMobileNo(String mobileNo){
        selectDropDownItem(mobileNoDropDown, mobileNo);
        LOGGER.info("Selected Mobile No : " + mobileNo + ".");
        return this;
    }

    public StarPointsPage clickStarPoints(){
        starPointsBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        StarPointsPage starPointsPage = new StarPointsPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), starPointsPage);
        LOGGER.info("Navigated StarPoints page.");
        return starPointsPage;
    }
}

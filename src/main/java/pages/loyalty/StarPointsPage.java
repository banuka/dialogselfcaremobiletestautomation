package pages.loyalty;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;

public class StarPointsPage extends BasePage {
    AppiumDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(StarPointsPage.class);

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"wallet\"]")
    private MobileElement balanceBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"sp_redeem\"]")
    private MobileElement redeemBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"sp_transfer\"]")
    private MobileElement transferBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"sp_donate\"]")
    private MobileElement donateBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"sp_history\"]")
    private MobileElement transactionHistoryBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Subscribe to Alerts\"]")
    private MobileElement subscribeToAlertsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Terms & Conditions\"]")
    private MobileElement termsAndConditionsBtn;

    public StarPointsPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Terms & Conditions\"]"));
    }


    public StarPointsBalance navigateToBalance() {
        balanceBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        StarPointsBalance starPointsBalance = new StarPointsBalance(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), starPointsBalance);
        LOGGER.info("Navigated to StarPoints Balance page.");
        return starPointsBalance;
    }

    public StarPointsTransferPage navigateToStarPointsTransfer(){
        transferBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        StarPointsTransferPage starPointsTransferPage = new StarPointsTransferPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), starPointsTransferPage);
        LOGGER.info("Navigated to StarPoints Transfer page.");
        return starPointsTransferPage;
    }

}

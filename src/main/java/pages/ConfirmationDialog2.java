package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ConfirmationDialog2 extends BasePage{

    private AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Yes\"]")
    private WebElement yesBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"No\"]")
    private WebElement noBtn;

    public ConfirmationDialog2(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"No\"]"));
    }

    public void clickYesBtn(){
        yesBtn.click();
    }

    public void clickNoBtn(){
        noBtn.click();
    }
}

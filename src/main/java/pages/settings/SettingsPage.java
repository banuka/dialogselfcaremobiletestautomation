package pages.settings;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.PageBase;
import pages.mobile.myprofile.MyProfilePage;

/**
 * @author Banuka Liyanage created on 1/26/2018
 */
public class SettingsPage extends PageBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsPage.class);
    private AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Change Language\"]")
    private WebElement changeLanguageBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Manage Accounts\"]")
    private WebElement manageAccountsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Dialog Guardian\"]")
    private WebElement dialogGuardianBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Page Transitions\"]")
    private WebElement pageTransitionsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Manage Ads\"]")
    private WebElement manageAdsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private WebElement backBtn;

    public SettingsPage(AppiumDriver driver) throws FrameworkException {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Manage Ads\"]"));
    }

    public MyAccountsPage navigateToMyAccounts() throws FrameworkException{
        manageAccountsBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        MyAccountsPage myAccountsPage = new MyAccountsPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), myAccountsPage);
        LOGGER.info("Navigated to MyAccounts.");
        return myAccountsPage;
    }
}

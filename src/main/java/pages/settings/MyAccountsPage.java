package pages.settings;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.PageBase;

/**
 * @author Banuka Liyanage created on 1/26/2018
 */
public class MyAccountsPage extends PageBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyAccountsPage.class);
    private AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Show By\"]")
    private WebElement showByBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Add Connection\"]")
    private WebElement addConnectionBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private WebElement backBtn;

    public MyAccountsPage(AppiumDriver driver) throws FrameworkException {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Add Connection\"]"));
    }
}

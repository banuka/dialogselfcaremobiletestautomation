package pages.login;

/**
 * @author Banuka Liyanage created on 19/20/2017
 */

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;


public class WellComePage extends BasePage {
    private AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"I Accept\"]")
    private WebElement stressTestResulsFilterButton;

    public WellComePage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"I Accept\"]"));
    }

    public LanguageSettingsPage clickIAccept() {
        stressTestResulsFilterButton.click();
        try {
            Thread.sleep(4500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LanguageSettingsPage languageSettingsPage = new LanguageSettingsPage(driver);
        PageFactory.initElements(driver, languageSettingsPage);
        return languageSettingsPage;
    }

}

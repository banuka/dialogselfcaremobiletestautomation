package pages.login;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pages.BasePage;

/**
 * @author Banuka Liyanage created on 10/20/2017
 */
public class AddNickNameDialog extends BasePage {
    private AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Continue\"]")
    private WebElement continueBtn;

    public AddNickNameDialog(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"OK\"]"));
    }

    public void addNickName(String nickName){

    }
}

package pages.login;

/**
 * @author Banuka Liyanage created on 19/20/2017
 */

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import pages.ConfirmationDialog;

public class LanguageSettingsPage extends BasePage {
    private AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"I Accept\"]")
    private MobileElement stressTestResulsFilterButton;

    @FindBy(how = How.XPATH, using = "//android.view.View[@resource-id=\"ext-element-83\"]")
    private MobileElement languageDropDown;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Set\"]")
    private MobileElement setBtn;

    public LanguageSettingsPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"I Accept\"]"));
    }

    private LanguageSettingsPage selectLanguage(String language){
        selectDropDownItem(languageDropDown, language);
        return this;
    }

    private void clickSetBtn(){
        setBtn.click();
    }

    public void setLanuage(String lanuage){
        selectLanguage(lanuage);
        clickSetBtn();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Checking your Account...\"]"));
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(driver);
        PageFactory.initElements(driver, confirmationDialog);
        confirmationDialog.clickOKBtn();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Adding Account...\"]"));
    }
}

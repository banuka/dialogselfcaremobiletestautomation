package pages.mobile.ringintones;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Banuka Liyanage created on 11/6/2017
 */
public class SongsListPage extends SongsSelectionPanelPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(SongsSelectionPanelPage.class);
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Previous\"]")
    private MobileElement previousBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Categories\"]")
    private MobileElement categoriesBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Next\"]")
    private MobileElement nextBtn;

    public SongsListPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Categories\"]"));
    }

    public SongSubscribePopUp navigateToSubscribeRingTone(String title){
        try {
            driver.findElementByXPath("//android.view.View[@content-desc=\"" + title + "\"]");
            driver.findElementByXPath("//android.view.View[@content-desc=\"" + title + "\"]/following-sibling::android.widget.Image[@content-desc=\"icon_list_disclosure\"]").click();
        }catch (NoSuchElementException e){
            LOGGER.error("Couldn't find the given song " + title + ".");
//            Assert.fail("The song " + title + " is not found in the song list." + e.getMessage());
        }

        SongSubscribePopUp songSubscribePopUp = new SongSubscribePopUp(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), songSubscribePopUp);

        return songSubscribePopUp;
    }

}

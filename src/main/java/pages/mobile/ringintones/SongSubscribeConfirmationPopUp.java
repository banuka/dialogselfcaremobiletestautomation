package pages.mobile.ringintones;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;

/**
 * @author Banuka Liyanage created on 11/8/2017
 */
public class SongSubscribeConfirmationPopUp extends BasePage {
    private static final Logger LOGGER = LoggerFactory.getLogger(SongSubscribeConfirmationPopUp.class);
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"OK\"]")
    private MobileElement ringToneActivateOKBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Subscribe\"]")
    private MobileElement subscribeBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Cancel\"]")
    private MobileElement cancelBtn;

    public SongSubscribeConfirmationPopUp(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Subscribe\"]"));
    }

    public SubscribeSuccessufullPopUp clickConfirm(){
        subscribeBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        SubscribeSuccessufullPopUp subscribeSuccessufullPopUp = new SubscribeSuccessufullPopUp(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), subscribeSuccessufullPopUp);
        return subscribeSuccessufullPopUp;
    }
}

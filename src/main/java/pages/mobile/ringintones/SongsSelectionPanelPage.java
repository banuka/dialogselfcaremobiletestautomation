package pages.mobile.ringintones;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import utils.SharedObjectKeys;

/**
 * @author Banuka Liyanage created on 11/6/2017
 */
public class SongsSelectionPanelPage extends BasePage{
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[2]")
    private MobileElement selectionDropDown;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[3]")
    private MobileElement serchBoxTxtBox;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"icon_list_disclosure\"]")
    private MobileElement iconDisclosureBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private MobileElement backBtn;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[1]")
    private MobileElement mobileNoDropDown;

    @FindBy(how = How.XPATH, using = "(//android.view.View[@content-desc=\"Songs\"])[3]")
    private MobileElement songsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Top 10\"]")
    private MobileElement top10Btn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"My Tones\"]")
    private MobileElement myTonesBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Recommended\"]")
    private MobileElement recommendedBtn;

    public SongsSelectionPanelPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.widget.Image[@content-desc=\"icon_list_disclosure\"]"));
    }

    public SongsSelectionPanelPage setSelectionCriteria(String selectiion){
        selectDropDownItem(selectionDropDown, selectiion);
        return this;
    }

    public SongsListPage selectItem(String item){
        serchBoxTxtBox.sendKeys(item);
        iconDisclosureBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        SongsListPage songsListPage = new SongsListPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), songsListPage);

        setSharedPage(SharedObjectKeys.SONGS_LIST_PAGE, songsListPage);
        return songsListPage;
    }

/*TODO
    Implement Navigation to the My Tones
 */
//    public SongsListPage navigateToMyTones(){}
}

package pages.mobile.ringintones;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;

public class ActivateConfirmPopUp extends BasePage {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActivateConfirmPopUp.class);
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Yes\"]")
    private MobileElement yesBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"No\"]")
    private MobileElement noBtn;

    public ActivateConfirmPopUp(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Activate?\"]"));
    }

}

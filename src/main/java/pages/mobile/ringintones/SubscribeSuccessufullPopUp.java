package pages.mobile.ringintones;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pages.BasePage;
import utils.SharedObjectKeys;

/**
 * @author Banuka Liyanage created on 11/8/2017
 */
public class SubscribeSuccessufullPopUp extends BasePage {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscribeSuccessufullPopUp.class);
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"OK\"]")
    private MobileElement okBtn;

    public SubscribeSuccessufullPopUp(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"OK\"]"));
    }

    public SongsListPage clickOK(){
        okBtn.click();
        return getSharedPage(SharedObjectKeys.SONGS_LIST_PAGE, SongsListPage.class);
    }

    public SubscribeSuccessufullPopUp verifySuccessFullMessage(String message){
        String actMsg = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/following-sibling::android.view.View").getAttribute("name");
        Assert.assertEquals(actMsg, message);
        LOGGER.info("Verify ring tone subscribe success message.");
        return this;
    }
}

package pages.mobile.ringintones;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author Banuka Liyanage created on 11/6/2017
 */
public class SongsCategoriesPage extends SongsSelectionPanelPage{
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Request Song\"]")
    private MobileElement requestSongBtn;

    public SongsCategoriesPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Request Song\"]"));
    }
}

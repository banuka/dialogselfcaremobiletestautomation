package pages.mobile.ringintones;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;

/**
 * @author Banuka Liyanage created on 11/7/2017
 */
public class SongSubscribePopUp extends BasePage {
    private static final Logger LOGGER = LoggerFactory.getLogger(SongSubscribePopUp.class);
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"D\"]")
    private MobileElement closeBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Gift\"]")
    private MobileElement giftBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Subscribe\"]")
    private MobileElement subscribeBtn;


    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[1]/android.widget.EditText[1]")
    private MobileElement codeLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[1]/android.widget.EditText[2]")
    private MobileElement songLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[1]/android.widget.EditText[3]")
    private MobileElement artistLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[1]/android.widget.EditText[4]")
    private MobileElement chargeLbl;

    public SongSubscribePopUp(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Subscribe\"]"));
    }

    public void closePopUp(){
        closeBtn.click();
        LOGGER.info("Closed song subscribe popUp.");
    }

    public SongSubscribeConfirmationPopUp clickSubscribe(){
        LOGGER.info("Ring Tone Code : " + codeLbl.getText());
        LOGGER.info("Ring Tone Song : " + songLbl.getText());
        LOGGER.info("Ring Tone Artist : " + artistLbl.getText());
        LOGGER.info("Ring Tone Charge(Rs) : " + chargeLbl.getText());
        subscribeBtn.click();
        LOGGER.info("Clicked subscribe button.");

        SongSubscribeConfirmationPopUp songSubscribeConfirmationPopUp = new SongSubscribeConfirmationPopUp(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), songSubscribeConfirmationPopUp);

        return songSubscribeConfirmationPopUp;
    }
}

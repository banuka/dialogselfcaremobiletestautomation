package pages.mobile.data;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pages.BasePage;

/**
 * @author Banuka Liyanage created on 10/30/2017
 */
public class DataCommonPage extends BasePage {
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Z\"]")
    private MobileElement usageBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"b\"]")
    private MobileElement availablePkgListBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"i\"]")
    private MobileElement extendBtn;

    public DataCommonPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"b\"]"));
    }


}

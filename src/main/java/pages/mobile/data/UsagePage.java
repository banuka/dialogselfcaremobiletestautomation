package pages.mobile.data;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pages.BasePage;

/**
 * @author Banuka Liyanage created on 10/30/2017
 */
public class UsagePage extends DataCommonPage{
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[contains(@content-desc, 'Expiry Date:')]")
    private MobileElement expiryDateLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[contains(@content-desc, '%')]")
    private MobileElement remainingPercentageLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[contains(@content-desc, 'used of')]")
    private MobileElement usedDataLbl;

    @FindBy(how = How.XPATH, using = "(//android.view.View[@content-desc=\"Extend\"])[1]")
    private MobileElement extendDataBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Change Data Package\"]")
    private MobileElement changeDataPkgBtn;

    public UsagePage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Change Data Package\"]"));
    }

}

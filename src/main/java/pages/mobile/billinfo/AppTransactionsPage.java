package pages.mobile.billinfo;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author Banuka Liyanage created on 10/24/2017
 */
public class AppTransactionsPage extends BillInfoCommonPage{
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Amount\"]")
    private MobileElement amountColumnHeaderLbl;

    public AppTransactionsPage(AppiumDriver driver) throws FrameworkException {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Amount\"]"));
    }
}

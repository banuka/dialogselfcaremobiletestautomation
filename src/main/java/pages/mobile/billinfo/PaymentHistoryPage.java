package pages.mobile.billinfo;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Banuka Liyanage created on 10/24/2017
 */
public class PaymentHistoryPage extends BillInfoCommonPage {
    AppiumDriver<MobileElement> driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Amount\"]")
    private MobileElement amountColumnHeaderLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]")
    private MobileElement dialogSelfCareWebView;

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentHistoryPage.class);

    public PaymentHistoryPage(AppiumDriver driver) throws FrameworkException {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Amount\"]"));
    }


    public JSONArray getPaymentHistory() {

        List<MobileElement> txDateTimeElms = dialogSelfCareWebView.findElements(By.xpath("//android.view.View[@content-desc='']"));


        JSONArray jArray = new JSONArray();

//        JsonArray value1 = Json.createArrayBuilder().add(
//                Json.createObjectBuilder()
//                        .add("type", "home")
//                        .add("number", "212 555-1234"))
//                .add(Json.createObjectBuilder()
//                        .add("type", "fax")
//                        .add("number", "646 555-4567"))
//                .build();

        int count = 1;
        int i = 0;
        for (int x = 0; x < txDateTimeElms.size(); x++) {
            MobileElement mobileElement = txDateTimeElms.get(x);
            String date = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[@content-desc=''][" + count + "]/android.view.View[1]").getAttribute("name");
            String time = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[@content-desc=''][" + count + "]/android.view.View[3]").getAttribute("name");

            String amount = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[@content-desc=''][" + count + "]/following-sibling::android.view.View[1]").getAttribute("name");
            String mode = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[@content-desc=''][" + count + "]/following-sibling::android.view.View[2]").getAttribute("name");
            String place = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.view.View[@content-desc=''][" + count + "]/following-sibling::android.view.View[3]").getAttribute("name");

            JSONObject json = new JSONObject();
            json.put("Date/Time", new JSONObject().put("Date", date).put("Time", time))
                    .put("Amount", amount)
                    .put("Mode", mode)
                    .put("Place", place);

            jArray.put(json);

//            System.out.println("index of row is : " + i);
            i++;
            count++;
        }

        LOGGER.info("The payment history : " + jArray);
        return jArray;
    }
}

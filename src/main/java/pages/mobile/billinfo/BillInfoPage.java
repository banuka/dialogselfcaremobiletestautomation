package pages.mobile.billinfo;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.LoggerFactory;
import org.testng.asserts.SoftAssert;
import pages.MobilePage;
import utils.support.APIHandler;
import utils.support.RestAPIHandler;
import utils.support.SOAPClient;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Banuka Liyanage created on 10/23/2017
 */
public class BillInfoPage extends BillInfoCommonPage {
    AppiumDriver driver;
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BillInfoPage.class);
    HashMap<String, String> billInfoUI = new HashMap<String, String>();

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Continue\"]")
    private MobileElement continueBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Last bill amount (Rs)\"]/following-sibling::android.widget.EditText")
    private MobileElement lastBillAmountLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"outstanding (Rs)\"]/ancestor::android.view.View/following-sibling::android.widget.EditText")
    private MobileElement currentOutstandingLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Credit limit (Rs)\"]/following-sibling::android.widget.EditText")
    private MobileElement creditLimitLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Bill date\"]/following-sibling::android.widget.EditText")
    private MobileElement billDateLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Due date\"]/following-sibling::android.widget.EditText")
    private MobileElement dueDateLbl;


    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Pay by*\"]/following-sibling::android.widget.EditText")
    private MobileElement payByLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Amount (Rs)*\"]/following-sibling::android.widget.EditText")
    private MobileElement amountLbl;

    public BillInfoPage(AppiumDriver driver) throws FrameworkException {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Continue\"]"));
        LOGGER.info("Navigated to BillInfo Page.");
    }


    public void getBillInfoDetails(){

        billInfoUI.put("Last bill amount (Rs)",lastBillAmountLbl.getText());
        billInfoUI.put("Current outstanding (Rs)", currentOutstandingLbl.getText());
        billInfoUI.put("Credit limit (Rs)", creditLimitLbl.getAttribute("name"));
        billInfoUI.put("Bill date", billDateLbl.getText());
        billInfoUI.put("Due date", dueDateLbl.getText());
        billInfoUI.put("Pay by*", payByLbl.getText());
        billInfoUI.put("Amount (Rs)*", amountLbl.getAttribute("name"));

        System.out.println(billInfoUI);

    }

    public BillInfoPage verfyBillInfoDetails(String refAccount){
//        JSONObject billInfoAPI = RestAPIHandler.getBillInfo();

        getBillInfoDetails();

        Map<String, Object> requestParams = new LinkedHashMap<String, Object>();
        Map<String, Object> responseParams = null;

        SOAPClient soapClient = new SOAPClient("http://172.26.29.29:8280/services/OmibioService.OmibioServiceHttpSoap11Endpoint?wsdl", "getBillInfo", "http://crm.dialog.lk/getBillingInfo");
        soapClient.setPREFERRED_PREFIX("soapenv");
        soapClient.setNAMESPACE("ccb");
        soapClient.setNAMESPACE_URI("http://crm.dialog.lk/CCBSWSBible/");
        requestParams.put("p_mobile_no", refAccount);
        responseParams= soapClient.getResponse(requestParams, true);
        String last_bill_amount = responseParams.get("o_last_bill").toString();


        soapClient = new SOAPClient("http://172.26.34.91:8280/services/DialogServices.DialogServicesHttpSoap11Endpoint?wsdl",
                "getBillPaymentReminder", "http://cmu.dialog.lk/DialogServices/getBillPaymentReminder");
        soapClient.setPREFERRED_PREFIX("soapenv");
        soapClient.setNAMESPACE("dial");
        soapClient.setNAMESPACE_URI("http://cmu.dialog.lk/DialogServices/");

        requestParams = new LinkedHashMap<String, Object>();
        requestParams.put("refAccount", refAccount);
        requestParams.put("LOB", "GSM");
        requestParams.put("token", "456");
        requestParams.put("IMEI", "9402970236468959");
        responseParams = soapClient.getResponse(requestParams, false);
        String creditOutStanding = responseParams.get("CurrentBalance").toString();


        soapClient = new SOAPClient("http://172.26.29.29:8280/services/OmibioService.OmibioServiceHttpSoap11Endpoint?wsdl",
                "getCreditLimitForContractId", "http://crm.dialog.lk/getCreditLimitForContractId");
        soapClient.setPREFERRED_PREFIX("soapenv");
        soapClient.setNAMESPACE("ccb");
        soapClient.setNAMESPACE_URI("http://crm.dialog.lk/CCBSWSBible/");

        requestParams = new LinkedHashMap<String, Object>();
        requestParams.put("p_contract_id", "33569751");
        responseParams =soapClient.getResponse(requestParams, true);
        String creditLimit = ((HashMap)responseParams.get("result")).get("p_total").toString();

        SoftAssert softAssertion= new SoftAssert();
        softAssertion.assertTrue(last_bill_amount.startsWith(billInfoUI.get("Last bill amount (Rs)").split("\\.")[0]), "Miss match last bill amount.");
        softAssertion.assertTrue(creditOutStanding.startsWith(billInfoUI.get("Current outstanding (Rs)").split("\\.")[0]), "Miss match Current outstanding.");
        softAssertion.assertEquals(creditLimit, billInfoUI.get("Credit limit (Rs)"), "Miss match Current outstanding.");

        softAssertion.assertAll();

        return this;
    }
}

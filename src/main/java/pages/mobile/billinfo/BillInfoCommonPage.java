package pages.mobile.billinfo;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import pages.MobilePage;
import pages.PageBase;

/**
 * @author Banuka Liyanage created on 10/24/2017
 */
public class BillInfoCommonPage extends PageBase{
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"i\"]")
    private MobileElement billInfoBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\".\"]")
    private MobileElement payBulkBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"l\"]")
    private MobileElement appTransactionBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"t\"]")
    private MobileElement paymentHistoryBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private MobileElement backBtn;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText")
    private MobileElement mobileNoDropDown;

    public BillInfoCommonPage(AppiumDriver driver) throws FrameworkException{
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"t\"]"));
    }


    public BillInfoPage clickOnBillInfo()throws FrameworkException{
        billInfoBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        BillInfoPage billInfoPage = new BillInfoPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), billInfoPage);
        return billInfoPage;
    }

    public PayBulkPage clickOnPayBulk()throws FrameworkException{
        payBulkBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        PayBulkPage payBulkPage = new PayBulkPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), payBulkPage);
        return payBulkPage;
    }

    public AppTransactionsPage clickOnAppTransactions()throws FrameworkException{
        appTransactionBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        AppTransactionsPage appTransactionsPage = new AppTransactionsPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), appTransactionsPage);
        return appTransactionsPage;
    }

    public PaymentHistoryPage clickOnPaymentHistory() throws FrameworkException{
        paymentHistoryBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        PaymentHistoryPage paymentHistoryPage = new PaymentHistoryPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), paymentHistoryPage);
        return paymentHistoryPage;
    }

    public MobilePage clickOnBackButton()throws FrameworkException{
        backBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        MobilePage mobilePage = new MobilePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), mobilePage);
        return mobilePage;
    }

    public BasePage selectMobileNo(String mobile) throws FrameworkException{
        if(mobile.equals(mobileNoDropDown.getText())) {
            selectDropDownItem(mobileNoDropDown, mobile);
            return this;
        }else {
            MobilePage mobilePage = new MobilePage(driver);
            PageFactory.initElements(new AppiumFieldDecorator(driver), mobilePage);
            return mobilePage;
        }
    }
}

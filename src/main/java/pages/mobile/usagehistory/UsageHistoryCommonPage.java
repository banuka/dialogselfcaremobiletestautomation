package pages.mobile.usagehistory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import pages.mobile.billinfo.PayBulkPage;

/**
 * @author Banuka Liyanage created on 10/31/2017
 */
public class UsageHistoryCommonPage extends BasePage{
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Calls\"]")
    private MobileElement callsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"M\"]")
    private MobileElement smsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"P\"]")
    private MobileElement mmsBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"G\"]")
    private MobileElement dataBtn;

    public UsageHistoryCommonPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"G\"]"));
    }


    public SMSPage clickOnSMS(){
        smsBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        SMSPage smsPage = new SMSPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), smsPage);
        return smsPage;
    }

}

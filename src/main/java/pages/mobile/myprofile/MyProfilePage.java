package pages.mobile.myprofile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.asserts.SoftAssert;
import pages.BasePage;
import utils.support.SOAPClient;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Banuka Liyanage created on 10/24/2017
 */
public class MyProfilePage extends BasePage {
    AppiumDriver driver;
    HashMap<String, String> myProfileInfo = new HashMap<String, String>();
    private static final Logger LOGGER = LoggerFactory.getLogger(MyProfilePage.class);

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[2]")
    private MobileElement connectionNoLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[3]")
    private MobileElement nameLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[4]")
    private MobileElement packageRentalLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[5]")
    private MobileElement connectionStatusLbl;

    @FindBy(how = How.XPATH, using = "\t//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[6]")
    private MobileElement addressLbl;

    public MyProfilePage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText[6]"));
        LOGGER.info("Navigated to MyProfile Page.");
    }


    public MyProfilePage getMyProfileDetails() {

        myProfileInfo.put("Connection #", connectionNoLbl.getText());
        myProfileInfo.put("Name", nameLbl.getAttribute("text"));
        myProfileInfo.put("Package Rental", packageRentalLbl.getText());
        myProfileInfo.put("Connection Status", connectionStatusLbl.getText());
        myProfileInfo.put("Address", addressLbl.getText());

        LOGGER.info("MyProfile : " + myProfileInfo);

        return this;
    }


    public MyProfilePage verifyMyProfileDetails() {

        Map<String, Object> requestParams = new LinkedHashMap<String, Object>();
        Map<String, Object> responseParams = null;

        SOAPClient soapClient = new SOAPClient("http://172.26.34.91:8280/services/DialogServices.DialogServicesHttpSoap11Endpoint?wsdl", "getCustomerDetails", "http://cmu.dialog.lk/DialogServices/getCustomerDetails");
        soapClient.setPREFERRED_PREFIX("soapenv");
        soapClient.setNAMESPACE("dial");
        soapClient.setNAMESPACE_URI("http://cmu.dialog.lk/DialogServices/");

        requestParams.put("refAccount", "777334187");
        requestParams.put("LOB", "GSM");
        requestParams.put("Token", "456");
        requestParams.put("IMEI", "9402970236468959");
        responseParams = soapClient.getResponse(requestParams, false);

        String name = responseParams.get("cust_name").toString();
        String pkgRental = responseParams.get("packageRental").toString();

        HashMap<String, Object> billingAddress = (HashMap<String, Object>)responseParams.get("billingAddress");
        String address = billingAddress.get("line1").toString() + " " + billingAddress.get("line2").toString() + " " + billingAddress.get("line3").toString() + " " + billingAddress.get("city").toString();

        SoftAssert softAssertion= new SoftAssert();
        softAssertion.assertEquals(myProfileInfo.get("Name"), name, "Miss match customer Name.");
        if(myProfileInfo.get("Connection Status").equals("Connected")) {
            softAssertion.assertEquals("C", responseParams.get("ConStatus").toString(), "Miss match customer Name.");
        }
        softAssertion.assertTrue(myProfileInfo.get("Address").replace("\n"," ").replace("(","").replace(")","").equals(address), "Miss match customer Address.");
        softAssertion.assertEquals(myProfileInfo.get("Package Rental"), pkgRental,"Miss match package rental.");

        softAssertion.assertAll();

        LOGGER.info("My Profile Details were verified.");
        return this;
    }
}

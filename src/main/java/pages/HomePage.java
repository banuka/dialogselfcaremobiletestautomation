package pages;

/**
 * @author Banuka Liyanage created on 19/20/2017
 */

import Exceptions.FrameworkException;
import com.sun.prism.impl.FactoryResetException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.homebroadband.HomeBroadBandPage;
import pages.loyalty.LoyaltyPage;
import pages.settings.SettingsPage;

public class HomePage extends BasePage {
    private AppiumDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePage.class);

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"games\"]")
    private MobileElement gamesBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"storelocator\"]")
    private MobileElement locateUsBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"help\"]")
    private MobileElement supportBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"exit\"]")
    private MobileElement exitBtn;

    public HomePage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.widget.Image[@content-desc=\"exit\"]"));
    }


    public MobilePage navigateToMobile()throws FrameworkException {
        (new TouchAction(driver)).tap(142, 366).perform();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        MobilePage mobilePage = new MobilePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), mobilePage);
        LOGGER.info("Navigated to Mobile.");
        return mobilePage;
    }

    public LoyaltyPage navigateToLoyalty() throws FrameworkException{
        (new TouchAction(driver)).tap(357, 703).perform();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        LoyaltyPage loyaltyPage = new LoyaltyPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), loyaltyPage);
        LOGGER.info("Navigated to Loyalty.");
        return loyaltyPage;
    }

    public HomeBroadBandPage navigateToHomeBroadBandFixedLine() throws FrameworkException{
        (new TouchAction(driver)).tap(550, 550).perform();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        HomeBroadBandPage homeBroadBandPage = new HomeBroadBandPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), homeBroadBandPage);
        LOGGER.info("Navigated to HomeBroadBand.");
        return homeBroadBandPage;
    }

    public SettingsPage navigateToSettings() throws FrameworkException{
        (new TouchAction(driver)).tap(673, 95).perform();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        SettingsPage settingsPage = new SettingsPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), settingsPage);
        LOGGER.info("Navigated to Settings.");
        return settingsPage;
    }
}

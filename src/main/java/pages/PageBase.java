package pages;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

/**
 * @author Banuka Liyanage created on 12/29/2017
 */
public class PageBase extends BasePage{
    private AppiumDriver driver;

    public PageBase(AppiumDriver driver) throws FrameworkException{
        super(driver);

        try {
            driver.findElement(By.xpath("//android.view.View[@content-desc=\"Connection Failure\"]"));
            logPageLoadingError("App_Loading_error", "Connection time Out.");
            throw new FrameworkException("Error loading page : " + this.getClass().getName());
        }catch (NoSuchElementException e){

        }
    }

}

package pages;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.LoggerFactory;
import pages.mobile.billinfo.BillInfoPage;
import pages.mobile.data.UsagePage;
import pages.mobile.myprofile.MyProfilePage;
import pages.mobile.ringintones.SongsCategoriesPage;
import pages.mobile.usagehistory.CallsPage;

import java.util.logging.Logger;

/**
 * @author Banuka Liyanage created on 10/23/2017
 */
public class MobilePage extends PageBase {
    AppiumDriver driver;
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(MobilePage.class);

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"bill\"]")
    private MobileElement billInfoBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"datausage\"]")
    private MobileElement dataBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"creditsvc\"]")
    private MobileElement creditServicesBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"icon-my-plan\"]")
    private MobileElement myPlanBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"vas\"]")
    private MobileElement VASBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"ringintone\"]")
    private MobileElement ringINTOnesBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"roaming\"]")
    private MobileElement roamingAndIDDBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"sharecredit\"]")
    private MobileElement sendCreditBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"charge_history\"]")
    private MobileElement usageHistoryBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"other-balace\"]")
    private MobileElement otherBalanceBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"profile\"]")
    private MobileElement myProfileBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"callservices\"]")
    private MobileElement callManagementBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"hybrid\"]")
    private MobileElement hybridBtn;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText")
    private MobileElement mobileNoDropDown;

    public MobilePage(AppiumDriver driver) throws FrameworkException{
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.widget.Image[@content-desc=\"callservices\"]"));
        LOGGER.info("Navigated to Mobile Page.");
    }


    public BillInfoPage navigateToBillInfo()throws FrameworkException{
        billInfoBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        BillInfoPage billInfoPage = new BillInfoPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), billInfoPage);
        return billInfoPage;
    }

    public MyProfilePage navigateToMyProfile() throws FrameworkException{
        myProfileBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        MyProfilePage myProfilePage = new MyProfilePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), myProfilePage);
        return myProfilePage;
    }

    public UsagePage navigateToData(){
        dataBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        UsagePage usagePage = new UsagePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), usagePage);
        return usagePage;
    }

    public CallsPage navigateToUsageHistory(){
        usageHistoryBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        CallsPage callsPage = new CallsPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), callsPage);
        return callsPage;
    }

    public SongsCategoriesPage navigateToRingINTones(){
        ringINTOnesBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        SongsCategoriesPage songsCategoriesPage = new SongsCategoriesPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), songsCategoriesPage);
        return songsCategoriesPage;
    }

    public MobilePage selectMobileNo(String mobileNo){
        selectDropDownItem(mobileNoDropDown, mobileNo);
        LOGGER.info("Select Mobile No " + mobileNo + " from mobile no dropdown.");
        return this;
    }
}
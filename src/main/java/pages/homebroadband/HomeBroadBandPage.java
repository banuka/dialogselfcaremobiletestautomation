package pages.homebroadband;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;
import pages.HomePage;
import pages.PageBase;
import pages.homebroadband.billinfo.BillInfoPage;
import pages.homebroadband.myprofile.MyProfilePage;
import pages.homebroadband.data.UsagePage;

public class HomeBroadBandPage extends PageBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeBroadBandPage.class);
    private AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"bill\"]")
    private WebElement billInfoBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"profile\"]")
    private WebElement myProfileBtn;

    @FindBy(how = How.XPATH, using = "//android.widget.Image[@content-desc=\"datausage\"]")
    private WebElement dataBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private WebElement backBtn;

    public HomeBroadBandPage(AppiumDriver driver) throws FrameworkException {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Back\"]"));
    }

    public BillInfoPage navigateToBillInfo() {
        billInfoBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        BillInfoPage billInfoPage = new BillInfoPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), billInfoPage);
        LOGGER.info("Navigated to BillInfo.");
        return billInfoPage;
    }

    public MyProfilePage navigateToMyProfile() {
        myProfileBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        MyProfilePage myProfilePage = new MyProfilePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), myProfilePage);
        LOGGER.info("Navigated to MyProfile.");
        return myProfilePage;
    }

    public UsagePage navigateToData() {
        dataBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        UsagePage usagePage = new UsagePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), usagePage);
        LOGGER.info("Navigated to Data.");
        return usagePage;
    }
}

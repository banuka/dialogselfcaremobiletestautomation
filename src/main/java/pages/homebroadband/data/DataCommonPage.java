package pages.homebroadband.data;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;

public class DataCommonPage extends BasePage {
    AppiumDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(DataCommonPage.class);

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Z\"]")
    private MobileElement usageBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"t\"]")
    private MobileElement historyBtn;

    @FindBy(how = How.XPATH, using = "\t//android.view.View[@content-desc=\"Extend\"]")
    private MobileElement extendBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private WebElement backBtn;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText")
    private WebElement mobileNoDropDown;

    public DataCommonPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"t\"]"));
    }


    public UsagePage clickUsageBtn(){
        usageBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        UsagePage usagePage = new UsagePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), usagePage);
        LOGGER.info("Navigated to Data Usage Page.");
        return usagePage;
    }

    public HistoryPage clickHistoryPage(){
        historyBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        HistoryPage historyPage = new HistoryPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), historyPage);
        LOGGER.info("Navigated to Data History Page.");
        return historyPage;
    }
}

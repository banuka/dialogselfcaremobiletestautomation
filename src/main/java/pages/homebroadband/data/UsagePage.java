package pages.homebroadband.data;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.homebroadband.HomeBroadBandPage.*;
import pages.homebroadband.myprofile.MyProfilePage;

import java.util.HashMap;

public class UsagePage extends DataCommonPage {
    AppiumDriver driver;
    HashMap<String, String> usageInfo = new HashMap<String, String>();
    private static final Logger LOGGER = LoggerFactory.getLogger(UsagePage.class);

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc='Any Time Data']/following-sibling::android.view.View[contains(@content-desc, 'Expiry Date:')]")
    private MobileElement expiryDateLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc='Any Time Data']/following-sibling::android.view.View[contains(@content-desc, '%')]")
    private MobileElement remainingPercentageLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc='Any Time Data']/following-sibling::android.view.View[contains(@content-desc, 'used of')]")
    private MobileElement usedDataLbl;

//    @FindBy(how = How.XPATH, using = "(//android.view.View[@content-desc=\"Extend\"])[1]")
//    private MobileElement extendDataBtn;
//android.view.View[@content-desc="Any Time Data"]/following-sibling::android.widget.EditText

    public UsagePage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[contains(@content-desc, 'used of')]"));
    }

    public UsagePage getHomeBroadUsageDetails() {

        usageInfo.put("Expiry Date:", expiryDateLbl.getAttribute("name"));
        usageInfo.put("remaining", remainingPercentageLbl.getAttribute("name"));
        usageInfo.put("used of remaining", usedDataLbl.getAttribute("name").split("used of")[0].trim());
        usageInfo.put("used of total", usedDataLbl.getAttribute("name").split("used of")[1].trim());

        LOGGER.info("Printing HomeBroadBand usageInfo details : " + usageInfo);

        return this;
    }
}

package pages.homebroadband.data;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HistoryPage extends DataCommonPage{
    AppiumDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(HistoryPage.class);

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Date\"]")
    private MobileElement dateColumnHeaderLbl;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]")
    private MobileElement dialogSelfCareWebView;

    public HistoryPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Date\"]"));
    }


    public JSONArray getDataHistory() {

        JSONArray jArray = new JSONArray();

        int count1 = 1;
        int count2 = 2;
        int count3 = 3;
        WebElement columnElm = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.Image[2]/following-sibling::android.view.View[" + count1 + "]");

        while (!columnElm.getAttribute("name").equals("Z")) {

            String dateTime   = columnElm.getAttribute("name");
            String quotaName = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.Image[2]/following-sibling::android.view.View[" + count2 + "]").getAttribute("name");
            String quotaUsage = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.Image[2]/following-sibling::android.view.View[" + count3 + "]").getAttribute("name");

            JSONObject json = new JSONObject()
                    .put("Date", dateTime)
                    .put("Quota Name", quotaName)
                    .put("Quota Usage", quotaUsage);

            jArray.put(json);

            count1 = count1 + 3;
            count2 = count2 + 3;
            count3 = count3 + 3;

            columnElm = driver.findElementByXPath("//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.Image[2]/following-sibling::android.view.View[" + count1 + "]");
            System.out.println("count1 is : " + count1);
        }

        LOGGER.info("Data history : " + jArray);
        return jArray;
    }

}

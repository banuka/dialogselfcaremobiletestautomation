package pages.homebroadband.myprofile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;

import java.util.HashMap;

public class MyProfilePage extends BasePage {
    AppiumDriver driver;
    HashMap<String, String> myProfileInfo = new HashMap<String, String>();
    private static final Logger LOGGER = LoggerFactory.getLogger(MyProfilePage.class);

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Connection #\"]/following-sibling::android.widget.EditText")
    private MobileElement connectionNoLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Name\"]/following-sibling::android.widget.EditText")
    private MobileElement nameLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Connection Status\"]/following-sibling::android.widget.EditText")
    private MobileElement connectionStatusLbl;

    public MyProfilePage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Connection Status\"]"));
    }


    public MyProfilePage getMyProfileDetails() {

        myProfileInfo.put("Connection #", connectionNoLbl.getText());
        myProfileInfo.put("Name", nameLbl.getAttribute("text"));
        myProfileInfo.put("Connection Status", connectionStatusLbl.getText());

        LOGGER.info("Printing HomeBroadBand MyProfile details : " + myProfileInfo);

        return this;
    }
}

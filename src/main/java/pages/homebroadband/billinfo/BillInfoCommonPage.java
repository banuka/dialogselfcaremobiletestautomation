package pages.homebroadband.billinfo;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import pages.mobile.billinfo.BillInfoPage;

public class BillInfoCommonPage extends BasePage {
    AppiumDriver driver;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"i\"]")
    private MobileElement billInfoBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\".\"]")
    private MobileElement payBulkBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"l\"]")
    private MobileElement appTransactionBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Back\"]")
    private MobileElement backBtn;

    @FindBy(how = How.XPATH, using = "//android.webkit.WebView[@content-desc=\"Dialog Selfcare\"]/android.widget.EditText")
    private MobileElement mobileNoDropDown;

    public BillInfoCommonPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"l\"]"));
    }


    public BillInfoPage clickOnBillInfo()throws FrameworkException {
        billInfoBtn.click();
        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
        BillInfoPage billInfoPage = new BillInfoPage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), billInfoPage);
        return billInfoPage;
    }

//    public PayBulkPage clickOnPayBulk(){
//        payBulkBtn.click();
//        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
//        PayBulkPage payBulkPage = new PayBulkPage(driver);
//        PageFactory.initElements(new AppiumFieldDecorator(driver), payBulkPage);
//        return payBulkPage;
//    }
//
//    public AppTransactionsPage clickOnAppTransactions(){
//        appTransactionBtn.click();
//        waitElementToBeDisappear(By.xpath("//android.view.View[@content-desc=\"Loading...\"]"));
//        AppTransactionsPage appTransactionsPage = new AppTransactionsPage(driver);
//        PageFactory.initElements(new AppiumFieldDecorator(driver), appTransactionsPage);
//        return appTransactionsPage;
//    }

}

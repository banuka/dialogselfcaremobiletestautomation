package pages.homebroadband.billinfo;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.homebroadband.billinfo.BillInfoCommonPage;

import java.util.HashMap;

public class BillInfoPage extends BillInfoCommonPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(BillInfoPage.class);
    AppiumDriver driver;
    HashMap<String, String> billInfoUI = new HashMap<String, String>();

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Continue\"]")
    private MobileElement continueBtn;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Last bill amount (Rs)\"]/following-sibling::android.widget.EditText")
    private MobileElement lastBillAmountLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"outstanding (Rs)\"]/ancestor::android.view.View/following-sibling::android.widget.EditText")
    private MobileElement currentOutstandingLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Credit limit (Rs)\"]/following-sibling::android.widget.EditText")
    private MobileElement creditLimitLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Bill date\"]/following-sibling::android.widget.EditText")
    private MobileElement billDateLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Due date\"]/following-sibling::android.widget.EditText")
    private MobileElement dueDateLbl;


    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Pay by*\"]/following-sibling::android.widget.EditText")
    private MobileElement payByLbl;

    @FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Amount (Rs)*\"]/following-sibling::android.widget.EditText")
    private MobileElement amountLbl;

    public BillInfoPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"Continue\"]"));
    }


    public void getBillInfoDetails(){

        billInfoUI.put("Last bill amount (Rs)",lastBillAmountLbl.getText());
        billInfoUI.put("Current outstanding (Rs)", currentOutstandingLbl.getText());
        billInfoUI.put("Credit limit (Rs)", creditLimitLbl.getAttribute("name"));
        billInfoUI.put("Bill date", billDateLbl.getText());
        billInfoUI.put("Due date", dueDateLbl.getText());
        billInfoUI.put("Pay by*", payByLbl.getText());
        billInfoUI.put("Amount (Rs)*", amountLbl.getAttribute("name"));

        LOGGER.info("Printing HomeBroadBand bill info : " + billInfoUI.toString());

    }
}

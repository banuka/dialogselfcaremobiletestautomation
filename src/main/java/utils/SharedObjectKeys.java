package utils;

import utils.support.SharedKey;

/**
 * @author Banuka Liyanage created on 12/29/2017
 */
public enum SharedObjectKeys implements SharedKey {
    HOME_PAGE, SONGS_LIST_PAGE
}

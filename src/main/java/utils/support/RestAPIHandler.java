package utils.support;

import com.jayway.restassured.http.ContentType;
import org.json.JSONObject;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.reset;
import static com.jayway.restassured.RestAssured.useRelaxedHTTPSValidation;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONObject;


public class RestAPIHandler {

    public static JSONObject getBillInfo() {

  /*
   * Given Accept the content in JSON format When I perform the GET request
   * with 777334187 Then Status code 200 ok should be returned
   *
   */

        JSONObject billInfo = new JSONObject();
        try {
            useRelaxedHTTPSValidation();
            com.jayway.restassured.response.Response response = given().accept(ContentType.JSON)
                    .header("Authorization", "Bearer 371bae3c-a399-313e-a4a4-0d8b8fa73516").
                            when().
                            get("https://mife.dialog.lk/apicall/crm/api/billing/v1.0.0/connections/777334187/bill/info?lob=GSM");

            JSONObject res_body = new JSONObject(response.getBody().print());
            billInfo = new JSONObject(res_body.get("getAllBillingInfoResponse").toString());
            System.out.println("ffff");
        } finally {
            reset();
        }

        return billInfo;
    }
}

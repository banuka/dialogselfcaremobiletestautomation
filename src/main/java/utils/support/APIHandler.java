package utils.support;

/**
 * @author Banuka Liyanage created on 10/26/2017
 */



import javax.xml.ws.WebServiceClient;

import java.io.IOException;




//import com.eviware.soapui.impl.WsdlInterfaceFactory;
//import com.eviware.soapui.impl.wsdl.WsdlInterface;
//import com.eviware.soapui.impl.wsdl.WsdlOperation;
//import com.eviware.soapui.impl.wsdl.WsdlProject;
//import com.eviware.soapui.impl.wsdl.WsdlRequest;
//import com.eviware.soapui.impl.wsdl.WsdlSubmit;
//import com.eviware.soapui.impl.wsdl.WsdlSubmitContext;
//import com.eviware.soapui.model.iface.Request.SubmitException;
//import com.eviware.soapui.support.SoapUIException;
//import com.eviware.soapui.support.SoapUIException;
//import org.apache.xmlbeans.XmlException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class APIHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(APIHandler.class);



//    public static void main(String[] args) throws Exception{
////        callSoapWebService("","");
////        call restServices
////        getBillInfo();
//
//        String wsdl = "http://172.26.34.91:8280/services/DialogServices.DialogServicesHttpSoap11Endpoint?wsdl";
//
//        String operation = "getCustomerDetails";
//
//        String requestStr = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:dial=\"http://cmu.dialog.lk/DialogServices/\">\n" +
//                "   <soap:Header/>\n" +
//                "   <soap:Body>\n" +
//                "      <dial:getCustomerDetailsRequest>\n" +
//                "         <refAccount>772461403</refAccount>\n" +
//                "         <LOB>GSM</LOB>\n" +
//                "         <Token>456</Token>\n" +
//                "         <IMEI>9402970236468959</IMEI>\n" +
//                "      </dial:getCustomerDetailsRequest>\n" +
//                "   </soap:Body>\n" +
//                "</soap:Envelope>";
//
//        sendServiceCall(wsdl, operation, requestStr);
//    }





//    public static String sendServiceCall(String wsdlPath, String operationName, String requestStr)
//            throws SoapUIException, XmlException, IOException, SubmitException {
//        LOGGER.info("Starting send the service call " + operationName);
//        // create new project
//        WsdlProject project = new WsdlProject();
//
//        // import amazon wsdl
//        WsdlInterface iface = WsdlInterfaceFactory.importWsdl(project, wsdlPath, true)[0];
//
//        // get desired operation
//        WsdlOperation operation = (WsdlOperation) iface.getOperationByName(operationName);
//
//        // create a new empty request for that operation
//        WsdlRequest request = operation.addNewRequest("operationName");
//
//        request.setRequestContent(requestStr);
//
//        // submit the request
//        @SuppressWarnings("rawtypes")
//        WsdlSubmit submit = (WsdlSubmit) request.submit(new WsdlSubmitContext(request), false);
//
//        // wait for the response
//        com.eviware.soapui.model.iface.Response response = submit.getResponse();
//        LOGGER.info("Service call sent successfully.  Response :");
//        LOGGER.info(response.getContentAsString());
//        return response.getContentAsString();
//    }

}

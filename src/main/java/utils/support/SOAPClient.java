package utils.support;

import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import java.util.*;

public class SOAPClient {

    private String PREFERRED_PREFIX = "soapenv";
    private String NAMESPACE = "ccb";
    private String NAMESPACE_URI = "http://crm.dialog.lk/CCBSWSBible/";
    private boolean PARAM_PREFIX_ENABLE = false;

    private HashMap<String, Object> responseParams = new HashMap<String, Object>();
    private Map<String, Object> requestParams = new LinkedHashMap<String, Object>();

    private String soapEndpointUrl;
    private String operation;
    private String soapAction;

    public SOAPClient(String soapEndpointUrl, String operation, String soapAction) {
        this.soapEndpointUrl = soapEndpointUrl;
        this.operation = operation;
        this.soapAction = soapAction;
    }

    public HashMap<String, Object> getResponse(Map<String, Object> requestParams, boolean withPreFix) {
//        String soapAction = "http://cmu.dialog.lk/DialogServices/getCustomerDetailsRequest";
//        String soapAction = NAMESPACE_URI + operation;
        this.requestParams = requestParams;
        this.PARAM_PREFIX_ENABLE = withPreFix;
        SOAPMessage soapResponse = callSoapWebService(soapEndpointUrl, soapAction);
        createResponseParams(soapResponse);
        return responseParams;
    }

    private void createSoapEnvelope(SOAPMessage soapMessage, Map<String, Object> requestParams) throws SOAPException {

        try {
            SOAPPart soapPart = soapMessage.getSOAPPart();

            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();

            SOAPHeader header = soapMessage.getSOAPHeader();
            SOAPBody body = soapMessage.getSOAPBody();
            envelope.removeNamespaceDeclaration(envelope.getPrefix());
            envelope.setPrefix(PREFERRED_PREFIX);
            header.setPrefix(PREFERRED_PREFIX);
            body.setPrefix(PREFERRED_PREFIX);

            envelope.addNamespaceDeclaration(NAMESPACE, NAMESPACE_URI);

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem = soapBody.addChildElement(this.operation + "Request", this.NAMESPACE, this.NAMESPACE_URI);


            Iterator it = requestParams.keySet().iterator();

            while (it.hasNext()) {
                String key = (String) it.next();
                Object value = requestParams.get(key);
                if (value instanceof Map) {
                    /**TODO
                     * implement code for the sub values.
                     */
                } else {
                    String value1 = (String) value;
                    SOAPElement soapBodyElem1 = PARAM_PREFIX_ENABLE ? soapBodyElem.addChildElement(key, NAMESPACE) : soapBodyElem.addChildElement(key);
                    soapBodyElem1.addTextNode(value1);
                }
            }
        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }

    private SOAPMessage callSoapWebService(String soapEndpointUrl, String soapAction) {
        SOAPMessage soapResponse = null;
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection.call(createSOAPRequest(soapAction, requestParams), soapEndpointUrl);

            // Print the SOAP Response
            System.out.println("Response SOAP Message:");
            soapResponse.writeTo(System.out);
            System.out.println();

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }

        return soapResponse;
    }

    private SOAPMessage createSOAPRequest(String soapAction, Map<String, Object> requestParams) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, requestParams);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("soapAction", this.soapAction);
        headers.addHeader("content-type", "text/xml");

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }


    private void createResponseParams(SOAPMessage message) {
        Name rvalue = null;
        SOAPPart soap = message.getSOAPPart();
        if (soap != null) {
            try {
                SOAPEnvelope envelope = soap.getEnvelope();
                if (envelope != null) {
                    SOAPBody body = envelope.getBody();
                    if (body != null) {
                        Name responseName = envelope.createName(operation + "Response", body.getFirstChild().getPrefix(), NAMESPACE_URI);
                        Iterator it = body.getChildElements(responseName);
                        Node responseNode = (Node) it.next();
                        NodeList responses = responseNode.getChildNodes();

                        for (int i = 0; i < responses.getLength(); i++) {
                            org.w3c.dom.Node node = responses.item(i);

                            if (node.getFirstChild().getFirstChild() == null) {
                                responseParams.put(node.getLocalName(), node.getTextContent());
                            } else {
                                addSubNodes(node);
                            }
                        }
                    }
                }
            } catch (SOAPException se) {
//                    LOGGER_logger.log(Level.FINE, "WSS: Unable to get SOAP envelope",
//                            se);
            }
        }
    }

    public void addSubNodes(org.w3c.dom.Node node) {
        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
            org.w3c.dom.Node childnode = node.getChildNodes().item(i);
            if (childnode.getFirstChild().getFirstChild() == null) {
                HashMap<String, String> tempMap;
                if (responseParams.get(childnode.getParentNode().getLocalName()) == null) {
                    tempMap = new HashMap<String, String>();
                    responseParams.put(childnode.getParentNode().getLocalName(), tempMap);
                } else {
                    tempMap = (HashMap<String, String>) responseParams.get(childnode.getParentNode().getLocalName());
                }
                tempMap.put(childnode.getLocalName(), childnode.getTextContent());
            } else {
                addSubNodes(childnode);
            }
        }
    }


    public void setPREFERRED_PREFIX(String PREFERRED_PREFIX) {
        this.PREFERRED_PREFIX = PREFERRED_PREFIX;
    }

    public void setNAMESPACE(String NAMESPACE) {
        this.NAMESPACE = NAMESPACE;
    }

    public void setNAMESPACE_URI(String NAMESPACE_URI) {
        this.NAMESPACE_URI = NAMESPACE_URI;
    }
}